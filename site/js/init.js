/*
* Component to implement autoscrolling in any component
* User will be able to reduce or increase speed
* Callbacks after function ends
 */

var speed = 30;
var modifier = 5;
var currentPositionY = 0;
var maxY = '';
var scrollComp = {};
var scrollCompId = 'js-scroller';
var scrollID = '';
var beginScroll  = function (e) {
	var lastFrame = Date.now();
	currentPositionY = window.pageYOffset;
	var scroller = function () {
		scrollID = requestAnimationFrame(scroller);
		var now = Date.now();
		var dt = (now - lastFrame) / 1000;
		lastFrame = now;
		currentPositionY += speed * dt;
		currentPositionY = currentPositionY < 0 ? 0 : currentPositionY;
		currentPositionY = currentPositionY > maxY ? maxY : currentPositionY;
		window.scrollTo(0, currentPositionY);
		if (currentPositionY === maxY) {
			scrollFinish();
			cancelAnimationFrame(scrollID);
		}
	};
	scroller();
};
var stopScroll = function (e) {
	if (scrollID) {
		cancelAnimationFrame(scrollID);
	}
};
var scrollFinish = function () {
	// reset variables
};
var increaseScrollSpeed = function () {
	speed = speed + modifier;
};
var decreaseScrollSpeed = function () {
	speed = speed - modifier;
};
var initScroller = function () {
    scrollComp = document.getElementById(scrollCompId);
    maxY = scrollComp.clientHeight;
    scrollComp.addEventListener('touchstart', stopScroll, false);
    scrollComp.addEventListener('touchend', beginScroll, false);
    scrollComp.addEventListener('mouseup', beginScroll, false);
    scrollComp.addEventListener('mousedown', stopScroll, false);
    //beginScroll();
};
//initScroller();

// end of scrolling component

angular.module('learningApp',['ngTouch'])
    .controller('learningController', ['$scope', '$timeout',function($scope, $timeout){
        var ctrl = this;
        var CLASS_FADE_OUT_LEFT = 'slide-fadeout-left';
        var CLASS_FADE_IN_LEFT = 'slide-fadein-left';
        var CLASS_FADE_OUT_RIGHT = 'slide-fadeout-right';
        var CLASS_FADE_IN_RIGHT = 'slide-fadein-right';
        ctrl.views = {
          landingPage: 'landingPage',
          story: 'story',
        };
        ctrl.currentViews = ctrl.views.landingPage;
        ctrl.curIndex = 0;
        ctrl.card = {};
        ctrl.story = [
            {
                type: 'content',
                image: 'images/shaadi.png',
                title: 'Sandeep ka Pyaar',
                description: 'Sandeep used to love Neha.                                                        Neha, however, on the other end, belonged to a high class rich family and had her own aspirations and desires. She was completely unaware of the fact how much Sandeep used to love her. It was a one sided attraction obviously. Sun arose, it was somewhere in the mid of June, scorching heat in North India, when Neha was caressing her long voluminous wet hair in the balcony of her 2 BHK. She was getting ready for her dance class. Sandeep, on the other hand, used to work as a chauffeur for a businessman.',
                time: 25000
            },
            {
                type: 'content',
                image: '',
                title: 'Title 2',
                description: 'But there was one thing common between both of them, they both used to love watching movies. On one fine day, coincidentally, both of them landed up in the same movie theatre for the same show.  . During the interval, they exchanged a glance and Sandeep’s heart beat pumped up. He courageously took a step forward to approach her but suddenly, the movie resumes. Neha sits back on her place comfortably. A story was running on the screen and a story was running inside Sandeep. He couldn’t regain the story after the interval. Multiple things were traveling through his brain and nerves. ‘THE END’, the screen says after sometime. Everyone leaves the hall except Sandeep. Sandeep, now, a bit tensed, takes out a beedi from his pocket and starts smoking. He suddenly hears a loud shouting voice coming from outside the cinema hall.',
                time: 20000
            },
            {
                type: 'question',
                question: 'The statements given below are taken from the aforementioned story. Choose the incorrect one out of the given options.\n',
                questionTitle:'Question 1',
                options: [
                    'The two leading characters in the above given story meet for the first time at a        picture palace.\n',
                    'Sandeep used to work as a driver for a businessman.',
                    'Sweltering, Blistering, Torrid and sub-zero are all synonyms of the word ‘Scorching’.\n',
                    '“It was a one sided attraction obviously” - usage of article in this particular sentence is appropriate.\n'
                ],
                solution: 'Statement 1 - The two leading characters in the story are Sandeep and Neha. A picture palace is nothing but a cinema hall only. Therefore, it can be clearly inferred from the 7th line of the paragraph that this statement is true. So, this is not the answer.\n' +
                '\n' +
                'Statement 2 - It is clearly mentioned in the 6th line of the above story that Sandeep used to work as a chauffeur for a businessman. \n' +
                '\n' +
                'Chauffeur => driver\n' +
                'This statement is true and hence not the answer.\n' +
                '\n' +
                'Statement 3 - Scorching means ‘very hot’.\n' +
                'Sweltering means ‘uncomfortably hot’.\n' +
                ' Blistering means ‘intense heat’.\n' +
                'Torrid means ‘very hot and dry’\n' +
                '\n' +
                'Sub-zero means ‘below zero degrees’\n' +
                '\n' +
                'So, out of the 4 given words, only sub-zero acts as an antonym of ‘Scorching’ and the rest are synonyms.\n' +
                '\n' +
                'So, this statement doesn’t hold true and hence the answer.\n' +
                '\n' +
                'Statement 4 - This statement holds true because usage of articles also depends upon the sound the succeeding word gives. Here, ‘one’ gives the sound of a consonant (vun) . Therefore, placement of ‘a’ before ‘one’ is apt and justified. This statement holds true and hence is not the answer.\n',
                answer: 0,
                chosenAnswer:-1
            },
            {
                type: 'question',
                question: 'Choose the correct synonym pair for the word "Coincidentally"',
                questionTitle:'Question 2',
                options: [
                    'Circumstantial- fortuitous',
                    'Fluky- Planned\n',
                    'Incidental- asynchronous\n',
                    'Intentional- foreseen'
                ],
                solution: 'Circumstantial- fortuitous => Both of these words mean the same as ‘Coincidentally’ i.e. unplanned and unintentional. Option 1 is the correct answer.\n' +
                '\n' +
                'Fluky- Planned => ‘Fluky’ is another synonym for ‘coincidentally’. So, this word will not form the correct pair with ‘planned’. Hence, this is not the answer.\n' +
                '\n' +
                'Incidental- asynchronous => ‘Incidental’ means ‘secondary’. ‘Asynchronous’ means ‘something which is not in sync’. This pair will not form the correct and required answer. We can drop this option too.\n' +
                '\n' +
                'Intentional- foreseen => This pair means something which is pre-planned and thought of and not happening by chance. This is not an apt answer for the asked question.\n',
                answer: 2,
                chosenAnswer:-1
            },
            {
                type: 'question',
                question: '“They didn’t have a clue about this”.  Choose the name of the punctuation mark used with the helping verb in the sentence from the given options.\n',
                questionTitle:'Question 3',
                options: [
                    'Single quotation mark\n',
                    'Exclamation mark\n',
                    'Apostrophe\n',
                    'Double quotation mark'
                ],
                solution: 'Firstly, we need to identify the helping verb in the given sentence. Here, ‘did’ is the helping verb used. Given punctuations can be denoted as:\n' +
                '                Single quotation mark => ‘ ’\n' +
                '                Exclamation mark =>  !\n' +
                '               Apostrophe => ’\n' +
                '               Double quotation mark => “ ”\n' +
                'Hence, option 3 is the correct answer.\n',
                answer: 2,
                chosenAnswer:-1
            },
	        {
		        type: 'question',
		        question: 'The past participle form of the verb "arise" is',
		        questionTitle:'Question 4',
		        options: [
			        'Arise',
			        'Arose',
			        'Arisen',
			        'Arosen'
		        ],
		        solution: 'The 3 verb forms of ‘arise’ are:\n' +
		        '\n' +
		        'Arise (1st form)\n' +
		        'Arose (2nd form)\n' +
		        'Arisen (3rd form)\n',
		        answer: 2,
		        chosenAnswer:-1
	        },
	        {
		        type: 'content',
		        image: '',
		        title: 'Sandeep ka Pyaar',
		        description: 'It was a teenage girl who was screaming and crying out in pain. She was bitten by a stray dog. Before Sandeep could do anything to help the little girl, a woman came into picture to assist the victim. It was none other than Neha. While Sandeep completely forgot the concern about the teenager, Neha arranged the paraphernalia to give her the first-aid. He followed them to the hospital and was peeping through the window to know about the events happening inside. ',
		        time: 25000
	        },
	        {
	            description:'Neha was treating the affected girl with utmost care. Sandeep looked at the scene with curiosity and things started building up in his mind. While all these things were going around, he got a call from his mother asking about his whereabouts. “Reaching home in another 15-20 mins”, Sandeep replied. Sandeep’s love for Neha kept on escalating day by day. He couldn’t control his emotions and decided to confess his feelings the next time he meets Neha. The day comes and Sandeep gets the opportunity. When he was about to approach her and pour out his sentiments, a little girl holds Neha’s hand and cutely says, “Mumma, let’s go and have some snacks”. Sandeep takes out his phone from the pocket and dials a number which says, “Wife”.',
		        type: 'content',
		        image: '',
		        title: 'Sandeep ka Pyaar',
		        time:25000
	        },
	        {
		        type: 'question',
		        question: 'Choose the SYNONYM of the given word.\n' +
		        '\n' +
		        '         Escalating\n',
		        questionTitle:'Question 1',
		        options: [
			        'Plunge',
			        'Shrink',
			        'Tumble',
			        'Mount'
		        ],
		        solution: '‘Plunge’ means to jump or dive.\n' +
		        '                 ‘Shrink’ means to become or make smaller in size or amount.\n' +
		        '                 ‘Tumble’ means to fall suddenly or collapse.\n' +
		        '                  ‘Mount’ means to go up or escalate.\n' +
		        'Hence, out of the above given meanings, option 4 i.e. Mount is the correct answer.\n',
		        answer: 3,
		        chosenAnswer:-1
	        },
	        {
		        type: 'question',
		        question: 'Choose the correct meaning of the word ‘Paraphernalia’.',
		        questionTitle:'Question 2',
		        options: [
			        'Equipment',
			        'Colossal',
			        'Humongous',
			        'Gluttony'
		        ],
		        solution: 'Equipment is the correct answer as ‘Paraphernalia’ means apparatus or equipment.\n' +
		        '\n' +
		        'The meanings of other words are as follows:\n' +
		        '‘Colossal’ means huge and massive \n' +
		        '‘Humongous’ is synonymous to colossal and means huge and enormous\n' +
		        'Gluttony means habitual greed or excess in eating\n' +
		        'Hence, option 1 is the correct answer.\n',
		        answer: 0,
		        chosenAnswer:-1
	        },
            {
                type: 'share'
            }
        ];
        ctrl.metrics = [
	        {
	        	"title":"Reading speed",
		        "text":"10 Words per minute",
		        color:"red",
		        progress:"50%"
	        },
	        {
		        title:"Grasping skills",
		        text:"2/6 Questions Correct",
		        color:"green",
		        progress:"40%"
	        }
        ];

	    ctrl.chosenAnswer = -1;
	    ctrl.loader = "70%";
	    ctrl.changeLoader = function(){
	    	ctrl.loader="100%";
	    };

	    ctrl.onOptionClick = function(index){
		    if(ctrl.chosenAnswer == -1)
			    ctrl.chosenAnswer = index;
	    }
        /*ctrl.question = {
            "question":"Some question",
            "options":[
                "hello blah blah blah sdfg dsf g dsfg dsfg sd fg dsfg  sdfg sdfg  dfsg sdfg ",
                "This is an option",
                "This is third option",
                "This is the fourth option"
            ],
            "solution":"asdfasdfasdf asdf asd asdf asd ads",
            "answer":1
        };
        ctrl.chosenAnswer = -1;
        ctrl.onOptionClick = function(index){
            if(ctrl.chosenAnswer == -1)
                ctrl.chosenAnswer = index;
        };*/

        // component to view and slide through questions
        var readingAnimation = function (time) {
            $timeout(function() {
                $('.js-bg-fill').height(0);
                $('.js-bg-fill').stop().animate({
                    'height': "100%"
                }, time);
            }, 0);
        };
        ctrl.previous = function () {
            if (ctrl.curIndex) {
                window.scrollTo(0, 0);
                var $testDiv = $('#banner');
                $testDiv.removeClass(CLASS_FADE_IN_LEFT);
                $testDiv.removeClass(CLASS_FADE_IN_RIGHT).addClass(CLASS_FADE_OUT_RIGHT);
                $timeout(function () {
                    ctrl.curIndex--;
                    ctrl.card = ctrl.story[ctrl.curIndex];
                    if (ctrl.card.type === 'content') {
                        $('#banner').css("background-color", '#FFFFFF');
                        readingAnimation(ctrl.card.time);
                    } else {
                        $('#banner').css("background-color", '#CFE1F4');
                        $('.js-bg-fill').height(0);
                        $('.js-bg-fill').stop();
                    }
                    $testDiv.removeClass(CLASS_FADE_OUT_RIGHT).addClass(CLASS_FADE_IN_RIGHT);
                }, 200);
            } else {
                ctrl.currentViews = ctrl.views.landingPage;
            }
        };

        ctrl.next = function () {
            if (ctrl.curIndex !== (ctrl.story.length - 1)) {
                window.scrollTo(0,0);
                var $testDiv = $('#banner');
                $testDiv.removeClass(CLASS_FADE_IN_RIGHT);
                $testDiv.removeClass(CLASS_FADE_IN_LEFT).addClass(CLASS_FADE_OUT_LEFT);
                $timeout(function () {
                    ctrl.curIndex++;
                    ctrl.card = ctrl.story[ctrl.curIndex];
                    if (ctrl.card.type === 'content') {
                        $('#banner').css("background-color", '#FFFFFF');
                        readingAnimation(ctrl.card.time);
                    } else {
                        $('#banner').css("background-color", '#CFE1F4');
                        $('.js-bg-fill').height(0);
                        $('.js-bg-fill').stop();
                    }
                    $testDiv.removeClass(CLASS_FADE_OUT_LEFT).addClass(CLASS_FADE_IN_LEFT);

	                if(ctrl.card.type == 'share') {
		                $(".my-rating").starRating({
			                starSize: 25,
			                callback: function (currentRating, $el) {
			                }
		                });
	                }
                }, 200);
            }

        };
        // end of sliding component

        var init = function () {
            ctrl.card = ctrl.story[0];
            if (ctrl.card.type === 'content') {
                readingAnimation(ctrl.card.time);
            }
        };

        // navigate to story
        ctrl.startStory = function () {
            ctrl.currentViews = ctrl.views.story;
            init();
        };

        $(document).ready(function(){
	        $('.sidenav').sidenav();
        });

	    $(".my-rating").starRating({
		    starSize: 25,
		    callback: function(currentRating, $el){
		    }
	    });



}]).directive('questionControl', function(){
	return {
		templateUrl:"/question.html",
		scope:{
			question:'='
		},
		link:function(scope, el, attr){
			scope.chosenAnswer = -1;
			scope.onOptionClick = function(index) {
                if (scope.question.chosenAnswer == -1){
                    scope.question.chosenAnswer = index;
                    $("html, body").animate({ scrollTop: $(document).height() }, 1000);
                }
			}
		}
	}
});